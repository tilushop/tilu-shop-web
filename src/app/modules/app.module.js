"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var forms_1 = require("@angular/forms");
var http_1 = require("@angular/http");
var app_routing_module_1 = require("./app-routing.module");
var app_component_1 = require("../my-app/app.component");
var items_component_1 = require("../items/items.component");
var items_to_order_component_1 = require("../items-to-order/items-to-order.component");
var item_detail_component_1 = require("../item-detail/item-detail.component");
var item_service_1 = require("../services/item.service");
var item_search_component_1 = require("../item-search/item-search.component");
var capitalize_pipe_1 = require("../pipes/capitalize.pipe");
var trim_pipe_1 = require("../pipes/trim.pipe");
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    core_1.NgModule({
        imports: [
            platform_browser_1.BrowserModule,
            forms_1.FormsModule,
            http_1.HttpModule,
            app_routing_module_1.AppRoutingModule,
            forms_1.ReactiveFormsModule
        ],
        exports: [capitalize_pipe_1.CapitalizePipe, trim_pipe_1.TrimPipe],
        declarations: [
            app_component_1.AppComponent,
            items_component_1.ItemsComponent,
            item_detail_component_1.ItemDetailComponent,
            items_to_order_component_1.ItemsToOrderComponent,
            item_search_component_1.ItemSearchComponent,
            capitalize_pipe_1.CapitalizePipe,
            trim_pipe_1.TrimPipe
        ],
        providers: [item_service_1.ItemService],
        bootstrap: [app_component_1.AppComponent]
    })
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map