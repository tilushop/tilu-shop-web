import {NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpModule} from "@angular/http";
import {AppRoutingModule} from "./app-routing.module";
import {AppComponent} from "../my-app/app.component";
import {ItemsComponent} from "../items/items.component";
import {ItemsToOrderComponent} from "../items-to-order/items-to-order.component";
import {ItemDetailComponent} from "../item-detail/item-detail.component";
import {ItemService} from "../services/item.service";
import {ItemSearchComponent} from "../item-search/item-search.component";
import {CapitalizePipe} from "../pipes/capitalize.pipe";
import {TrimPipe} from "../pipes/trim.pipe";

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,
    ReactiveFormsModule
  ],
  exports: [CapitalizePipe, TrimPipe],
  declarations: [
    AppComponent,
    ItemsComponent,
    ItemDetailComponent,
    ItemsToOrderComponent,
    ItemSearchComponent,
    CapitalizePipe,
    TrimPipe
  ],
  providers: [ItemService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
