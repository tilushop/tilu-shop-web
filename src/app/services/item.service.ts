import {Injectable} from "@angular/core";
import {Headers, Http} from "@angular/http";
import "rxjs/add/operator/toPromise";
import {Item} from "../models/item";
import {Order} from "../models/order";
import {ErrorHandler} from "./utils/ErrorHandler";
import {User} from "../models/user";

@Injectable()
export class ItemService {

  private headers = new Headers({'Content-Type': 'application/json'});
  private itemServiceUrl = 'http://localhost:9001/items';
  private orderServiceUrl = 'http://localhost:9002/orders';

  orderedItems: Item[] = [];

  constructor(private http: Http) {
  }

  getAllItems(): Promise<Item[]> {
    return this.http.get(this.itemServiceUrl)
    .toPromise()
    .then(response => <Item[]> response.json()
    )
    .catch(ErrorHandler.handleError);
  }

  getItem(id: string): Promise<Item> {
    const url = `${this.itemServiceUrl}/${id}`;
    return this.http.get(url)
    .toPromise()
    .then(response => <Item> response.json())
    .catch(ErrorHandler.handleError);
  }

  remove(id: string): Promise<void> {
    const url = `${this.itemServiceUrl}/${id}`;
    return this.http.delete(url, {headers: this.headers})
    .toPromise()
    .then(() => null)
    .catch(ErrorHandler.handleError);
  }

  create(item: Item): Promise<string> {
    return this.http
    .post(this.itemServiceUrl, JSON.stringify(item), {headers: this.headers})
    .toPromise()
    .then(res => res.json().data.id)
    .catch(ErrorHandler.handleError);
  }

  update(item: Item): Promise<void> {
    const url = `${this.itemServiceUrl}/${item.id}`;
    return this.http
    .put(url, JSON.stringify(item), {headers: this.headers})
    .toPromise()
    .then(() => null)
    .catch(ErrorHandler.handleError);
  }

  getAllItemCategories(): Promise<string[]> {
    return this.http.get(`${this.itemServiceUrl}/categories`)
    .toPromise()
    .then(response => <string[]> response.json()
    )
    .catch(ErrorHandler.handleError);
  }

  addToOrderList(item: Item) {
    this.orderedItems.push(item);
  }

  updateReservedQuantity(item: Item, value: number): Promise<void> {
    const url = `${this.itemServiceUrl}/${item.id}/quantity?field=reservedQuantity`;
    return this.http
    .put(url, JSON.stringify({value: value}), {headers: this.headers})
    .toPromise()
    .then(() => null)
    .catch(ErrorHandler.handleError);
  }

  sendOrderList(userData: any): Promise<void> {
    const url = `${this.orderServiceUrl}`;
    let order = new Order();

    let user = new User();
    user.name = userData.name;
    user.facebook = userData.facebook;
    user.email = userData.email;
    // user.birthDay = userData.birthDay;
    user.phone = userData.phone;
    user.address = userData.address;

    order.user = user;
    order.itemIds = this.orderedItems.map(x => x.id);
    order.orderNotes = "This is default note";

    this.orderedItems = [];

    return this.http
    .post(url, JSON.stringify(order), {headers: this.headers})
    .toPromise()
    .then(() => null)
    .catch(ErrorHandler.handleError);
  }
}

