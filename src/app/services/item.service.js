"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
require("rxjs/add/operator/toPromise");
var order_1 = require("../models/order");
var ErrorHandler_1 = require("./utils/ErrorHandler");
var user_1 = require("../models/user");
var ItemService = (function () {
    function ItemService(http) {
        this.http = http;
        this.headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        this.itemServiceUrl = 'http://localhost:9001/items';
        this.orderServiceUrl = 'http://localhost:9002/orders';
        this.orderedItems = [];
    }
    ItemService.prototype.getAllItems = function () {
        return this.http.get(this.itemServiceUrl)
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(ErrorHandler_1.ErrorHandler.handleError);
    };
    ItemService.prototype.getItem = function (id) {
        var url = this.itemServiceUrl + "/" + id;
        return this.http.get(url)
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(ErrorHandler_1.ErrorHandler.handleError);
    };
    ItemService.prototype.remove = function (id) {
        var url = this.itemServiceUrl + "/" + id;
        return this.http.delete(url, { headers: this.headers })
            .toPromise()
            .then(function () { return null; })
            .catch(ErrorHandler_1.ErrorHandler.handleError);
    };
    ItemService.prototype.create = function (item) {
        return this.http
            .post(this.itemServiceUrl, JSON.stringify(item), { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json().data.id; })
            .catch(ErrorHandler_1.ErrorHandler.handleError);
    };
    ItemService.prototype.update = function (item) {
        var url = this.itemServiceUrl + "/" + item.id;
        return this.http
            .put(url, JSON.stringify(item), { headers: this.headers })
            .toPromise()
            .then(function () { return null; })
            .catch(ErrorHandler_1.ErrorHandler.handleError);
    };
    ItemService.prototype.getAllItemCategories = function () {
        return this.http.get(this.itemServiceUrl + "/categories")
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(ErrorHandler_1.ErrorHandler.handleError);
    };
    ItemService.prototype.addToOrderList = function (item) {
        this.orderedItems.push(item);
    };
    ItemService.prototype.updateReservedQuantity = function (item, value) {
        var url = this.itemServiceUrl + "/" + item.id + "/quantity?field=reservedQuantity";
        return this.http
            .put(url, JSON.stringify({ value: value }), { headers: this.headers })
            .toPromise()
            .then(function () { return null; })
            .catch(ErrorHandler_1.ErrorHandler.handleError);
    };
    ItemService.prototype.sendOrderList = function (userData) {
        var url = "" + this.orderServiceUrl;
        var order = new order_1.Order();
        var user = new user_1.User();
        user.name = userData.name;
        user.facebook = userData.facebook;
        user.email = userData.email;
        // user.birthDay = userData.birthDay;
        user.phone = userData.phone;
        user.address = userData.address;
        order.user = user;
        order.itemIds = this.orderedItems.map(function (x) { return x.id; });
        order.orderNotes = "This is default note";
        this.orderedItems = [];
        return this.http
            .post(url, JSON.stringify(order), { headers: this.headers })
            .toPromise()
            .then(function () { return null; })
            .catch(ErrorHandler_1.ErrorHandler.handleError);
    };
    return ItemService;
}());
ItemService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], ItemService);
exports.ItemService = ItemService;
//# sourceMappingURL=item.service.js.map