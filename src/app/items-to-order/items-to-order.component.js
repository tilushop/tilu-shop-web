"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var item_service_1 = require("../services/item.service");
var forms_1 = require("@angular/forms");
var ItemsToOrderComponent = (function () {
    function ItemsToOrderComponent(itemService, router, fb) {
        this.itemService = itemService;
        this.router = router;
        this.fb = fb;
        this.items = [];
        this.events = [];
    }
    ItemsToOrderComponent.prototype.ngOnInit = function () {
        this.items = this.itemService.orderedItems;
        function EmailValidator(c) {
            var EMAIL_REGEX = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
            if (c.value.length > 0 && (c.value.length <= 5 || !EMAIL_REGEX.test(c.value))) {
                return {
                    noEmail: true
                };
            }
            // Null means valid, believe it or not
            return null;
        }
        function FacebookValidator(c) {
            if (c.value.indexOf("facebook.com/") < 0) {
                return {
                    noFacebook: true
                };
            }
            return null;
        }
        function PhoneValidator(c) {
            var PHONE_REGEX = /^(01[2689]|09)[0-9]{8}$/i;
            if (!PHONE_REGEX.test(c.value)) {
                return {
                    noPhone: true
                };
            }
            return null;
        }
        this.userDetailForm = this.fb.group({
            name: ['', [forms_1.Validators.required, forms_1.Validators.minLength(5)]],
            facebook: ['', [forms_1.Validators.required, FacebookValidator]],
            email: ['', EmailValidator],
            birthday: [''],
            phone: ['', [forms_1.Validators.required, PhoneValidator]],
            address: ['', [forms_1.Validators.required, forms_1.Validators.minLength(10)]]
        });
    };
    ItemsToOrderComponent.prototype.add = function (item) {
        this.itemService.create(item)
            .then(function (item) {
        });
    };
    ItemsToOrderComponent.prototype.removeItemFromOrderList = function (item) {
        this.itemService.orderedItems = this.itemService.orderedItems.filter(function (obj) { return obj !== item; });
        this.items = this.itemService.orderedItems;
        this.itemService
            .updateReservedQuantity(item, -1)
            .then(function () {
        });
    };
    ItemsToOrderComponent.prototype.onSelect = function (item) {
        this.selectedItem = item;
    };
    ItemsToOrderComponent.prototype.gotoDetail = function () {
        this.router.navigate(['/items', this.selectedItem.id]);
    };
    ItemsToOrderComponent.prototype.sendOrderList = function (formData, isValid) {
        this.submitted = true;
        if (isValid) {
            this.itemService.sendOrderList(formData)
                .then(function (result) {
            });
            this.items = [];
        }
    };
    return ItemsToOrderComponent;
}());
ItemsToOrderComponent = __decorate([
    core_1.Component({
        selector: 'my-items-to-order',
        templateUrl: './items-to-order.component.html',
        styleUrls: ['./items-to-orders.component.css']
    }),
    __metadata("design:paramtypes", [item_service_1.ItemService,
        router_1.Router,
        forms_1.FormBuilder])
], ItemsToOrderComponent);
exports.ItemsToOrderComponent = ItemsToOrderComponent;
//# sourceMappingURL=items-to-order.component.js.map