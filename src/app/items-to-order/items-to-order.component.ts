import {Component, OnInit} from "@angular/core";
import {Router} from "@angular/router";
import {Item} from "../models/item";
import {ItemService} from "../services/item.service";
import {Validators, FormBuilder, FormGroup, FormControl} from "@angular/forms";
@Component({
  selector: 'my-items-to-order',
  templateUrl: './items-to-order.component.html',
  styleUrls: ['./items-to-orders.component.css']
})
export class ItemsToOrderComponent implements OnInit {
  items: Item[] = [];
  selectedItem: Item;
  public userDetailForm: FormGroup;
  public submitted: boolean;
  public events: any[] = [];

  constructor(private itemService: ItemService,
              private router: Router,
              public fb: FormBuilder) {
  }

  ngOnInit(): void {
    this.items = this.itemService.orderedItems;

    function EmailValidator(c: FormControl) {
      let EMAIL_REGEX = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
      if (c.value.length > 0 && (c.value.length <= 5 || !EMAIL_REGEX.test(c.value))) {
        return {
          noEmail: true
        }
      }

      // Null means valid, believe it or not
      return null
    }

    function FacebookValidator(c: FormControl) {
      if (c.value.indexOf("facebook.com/") < 0) {
        return {
          noFacebook: true
        }
      }

      return null
    }

    function PhoneValidator(c: FormControl) {
      let PHONE_REGEX = /^(01[2689]|09)[0-9]{8}$/i;
      if (!PHONE_REGEX.test(c.value)) {
        return {
          noPhone: true
        }
      }
      return null
    }

    this.userDetailForm = this.fb.group({
      name: ['', [<any>Validators.required, <any>Validators.minLength(5)]],
      facebook: ['', [<any>Validators.required, FacebookValidator]],
      email: ['', EmailValidator],
      birthday: [''],
      phone: ['', [<any>Validators.required, PhoneValidator]],
      address: ['', [<any>Validators.required, <any>Validators.minLength(10)]]
    });

  }

  add(item: Item): void {
    this.itemService.create(item)
    .then(item => {
    });
  }

  removeItemFromOrderList(item: Item): void {
    this.itemService.orderedItems = this.itemService.orderedItems.filter(obj => obj !== item);
    this.items = this.itemService.orderedItems;
    this.itemService
    .updateReservedQuantity(item, -1)
    .then(() => {
    });
  }

  onSelect(item: Item): void {
    this.selectedItem = item;
  }

  gotoDetail(): void {
    this.router.navigate(['/items', this.selectedItem.id]);
  }

  sendOrderList(formData: any, isValid: boolean): void {
    this.submitted = true;
    if (isValid) {
      this.itemService.sendOrderList(formData)
      .then(result => {
      });
      this.items = [];
    }
  }


}
