export class Item {
  id: string;
  name: string;
  imageUrl: string;
  code: string;
  country: string;
  weight: number;
  description: string;
  basePrice: number;
  normalPrice: number;
  promotionPrice: number;
  active: boolean;
  orderQuantity: number;
  category: string;
  createdDate: Date;
  updatedDate: Date;
}
