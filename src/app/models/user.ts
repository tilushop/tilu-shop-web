export class User {
  id: string;
  name: string;
  facebook: string;
  email: string;
  birthDay: Date;
  phone: number;
  address: string;
  createdDate: Date;
  updatedDate: Date;
}