import {User} from "./user";
export class Order {
  id: string;
  user: User;
  itemIds: string[];
  orderNotes: string;
  createdDate: Date;
  updatedDate: Date;
}