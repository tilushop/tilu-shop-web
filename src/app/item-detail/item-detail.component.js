"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
require("rxjs/add/operator/switchMap");
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var common_1 = require("@angular/common");
var item_service_1 = require("../services/item.service");
var ItemDetailComponent = (function () {
    function ItemDetailComponent(itemService, route, location) {
        this.itemService = itemService;
        this.route = route;
        this.location = location;
    }
    ItemDetailComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.isItemAlreadyAdded = false;
        this.orderQuantity = 1;
        this.route.params
            .switchMap(function (params) { return _this.itemService.getItem(params['id']); })
            .subscribe(function (item) { return _this.item = item; });
    };
    ItemDetailComponent.prototype.goBack = function () {
        this.isItemAlreadyAdded = false;
        this.location.back();
    };
    ItemDetailComponent.prototype.addToOrderList = function () {
        var _this = this;
        if (!this.checkItemAdded(this.item)) {
            this.item.orderQuantity = this.orderQuantity;
            this.itemService
                .updateReservedQuantity(this.item, this.orderQuantity)
                .then(function () { return _this.itemService.addToOrderList(_this.item); });
        }
        else {
            this.isItemAlreadyAdded = true;
        }
    };
    ItemDetailComponent.prototype.checkItemAdded = function (item) {
        var orderedItems = this.itemService.orderedItems;
        if (orderedItems.length == 0) {
            return false;
        }
        else {
            var itemIds = orderedItems.map(function (item) { return item.id; });
            return itemIds.indexOf(item.id) > -1;
        }
    };
    ItemDetailComponent.prototype.isOrderQuantityValid = function () {
        return this.orderQuantity != null && this.orderQuantity > 0;
    };
    return ItemDetailComponent;
}());
ItemDetailComponent = __decorate([
    core_1.Component({
        selector: 'item-detail',
        templateUrl: './item-detail.component.html',
        styleUrls: ['./item-detail.component.css']
    }),
    __metadata("design:paramtypes", [item_service_1.ItemService,
        router_1.ActivatedRoute,
        common_1.Location])
], ItemDetailComponent);
exports.ItemDetailComponent = ItemDetailComponent;
//# sourceMappingURL=item-detail.component.js.map