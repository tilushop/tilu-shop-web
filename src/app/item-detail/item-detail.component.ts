import "rxjs/add/operator/switchMap";
import {Component, OnInit} from "@angular/core";
import {ActivatedRoute, Params} from "@angular/router";
import {Location} from "@angular/common";
import {Item} from "../models/item";
import {ItemService} from "../services/item.service";

@Component({
  selector: 'item-detail',
  templateUrl: './item-detail.component.html',
  styleUrls: ['./item-detail.component.css']
})
export class ItemDetailComponent implements OnInit {
  item: Item;
  isItemAlreadyAdded: boolean;
  orderQuantity: number;

  constructor(private itemService: ItemService,
              private route: ActivatedRoute,
              private location: Location) {
  }

  ngOnInit(): void {
    this.isItemAlreadyAdded = false;
    this.orderQuantity = 1;
    this.route.params
    .switchMap((params: Params) => this.itemService.getItem(params['id']))
    .subscribe(item => this.item = item);
  }

  goBack(): void {
    this.isItemAlreadyAdded = false;
    this.location.back();
  }

  addToOrderList(): void {
    if (!this.checkItemAdded(this.item)) {
      this.item.orderQuantity = this.orderQuantity;
      this.itemService
      .updateReservedQuantity(this.item, this.orderQuantity)
      .then(() => this.itemService.addToOrderList(this.item));
    } else {
      this.isItemAlreadyAdded = true;
    }
  }

  private checkItemAdded(item: Item) {
    let orderedItems = this.itemService.orderedItems;
    if (orderedItems.length == 0) {
      return false;
    } else {
      let itemIds = orderedItems.map(item => item.id);
      return itemIds.indexOf(item.id) > -1;
    }
  }

  private isOrderQuantityValid(): boolean {
    return this.orderQuantity != null && this.orderQuantity > 0;
  }
}
