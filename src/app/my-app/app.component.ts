import {Component} from "@angular/core";

@Component({
  selector: 'my-app',
  template: `
    <h2>{{title}}</h2>
    <nav>
      <a routerLink="/items" routerLinkActive="active">All Items</a>
      <a routerLink="/items/order" routerLinkActive="active">My Cart</a>
    </nav>
    <router-outlet></router-outlet>
  `,
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Tilu Shop';
}
