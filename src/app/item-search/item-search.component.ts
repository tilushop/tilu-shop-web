import {Component, OnInit, Input} from "@angular/core";
import {Router} from "@angular/router";
import {Observable} from "rxjs/Observable";
import {Subject} from "rxjs/Subject";
import "rxjs/add/observable/of";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/debounceTime";
import "rxjs/add/operator/distinctUntilChanged";
import {ItemSearchService} from "./item-search.service";
import {Item} from "../models/item";
import {ItemService} from "../services/item.service";

@Component({
  selector: 'item-search',
  templateUrl: './item-search.component.html',
  styleUrls: ['./item-search.component.css'],
  providers: [ItemSearchService]
})
export class ItemSearchComponent implements OnInit {
  items: Observable<Item[]>;
  private searchTerms = new Subject<string>();
  @Input('filteredItems')
  filteredItems: Observable<Item[]>;

  constructor(private itemSearchService: ItemSearchService,
              private itemService: ItemService,
              private router: Router) {
  }

  // Push a search term into the observable stream.
  search(term: string): void {
    this.searchTerms.next(term);
  }

  ngOnInit(): void {
    this.items = this.searchTerms
    .debounceTime(300)        // wait 300ms after each keystroke before considering the term
    .distinctUntilChanged()   // ignore if next search term is same as previous
    .switchMap(term => term ? this.itemSearchService.search(term) : this.itemService.getAllItems())
    .catch(error => {
      console.log(error);
      return Observable.of<Item[]>([]);
    });
  }

  gotoDetail(item: Item): void {
    let link = ['/items', item.id];
    this.router.navigate(link);
  }
}
