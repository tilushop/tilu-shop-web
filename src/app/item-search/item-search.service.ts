import {Injectable} from "@angular/core";
import {Http} from "@angular/http";
import "rxjs/add/operator/map";
import {Item} from "../models/item";
import {ErrorHandler} from "../services/utils/ErrorHandler";

@Injectable()
export class ItemSearchService {
  private itemServiceUrl = 'http://localhost:9001/items';

  constructor(private http: Http) {
  }

  search(term: string): Promise<Item[]> {
    return this.http.get(`${this.itemServiceUrl}/search?key=${term}`)
    .toPromise()
    .then(response => <Item[]> response.json()
    )
    .catch(ErrorHandler.handleError);
  }
}
