"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
require("rxjs/add/operator/map");
var ErrorHandler_1 = require("../services/utils/ErrorHandler");
var ItemSearchService = (function () {
    function ItemSearchService(http) {
        this.http = http;
        this.itemServiceUrl = 'http://localhost:9001/items';
    }
    ItemSearchService.prototype.search = function (term) {
        return this.http.get(this.itemServiceUrl + "/search?key=" + term)
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(ErrorHandler_1.ErrorHandler.handleError);
    };
    return ItemSearchService;
}());
ItemSearchService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], ItemSearchService);
exports.ItemSearchService = ItemSearchService;
//# sourceMappingURL=item-search.service.js.map