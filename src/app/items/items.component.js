"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var item_service_1 = require("../services/item.service");
var ItemsComponent = (function () {
    function ItemsComponent(itemService) {
        this.itemService = itemService;
        this.allItems = [];
        this.afilteredItems = [];
        this.itemCategories = [];
    }
    ItemsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.itemService
            .getAllItems()
            .then(function (items) {
            _this.allItems = items;
            _this.afilteredItems = items;
        });
        this.itemService
            .getAllItemCategories()
            .then(function (itemCategories) { return _this.itemCategories = _this.itemCategories.concat(itemCategories); });
    };
    ItemsComponent.prototype.onSelect = function (selectedCategory) {
        if (selectedCategory == "0") {
            this.afilteredItems = this.allItems;
        }
        else {
            this.afilteredItems = this.allItems.filter(function (item) { return item.category == selectedCategory; });
        }
    };
    return ItemsComponent;
}());
ItemsComponent = __decorate([
    core_1.Component({
        selector: 'my-items',
        templateUrl: './items.component.html',
        styleUrls: ['./items.component.css']
    }),
    __metadata("design:paramtypes", [item_service_1.ItemService])
], ItemsComponent);
exports.ItemsComponent = ItemsComponent;
//# sourceMappingURL=items.component.js.map