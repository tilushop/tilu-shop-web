import {Component, OnInit} from "@angular/core";
import {Item} from "../models/item";
import {ItemService} from "../services/item.service";

@Component({
  selector: 'my-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.css']
})
export class ItemsComponent implements OnInit {
  allItems: Item[] = [];
  afilteredItems: Item[] = [];
  itemCategories: string[] = [];

  constructor(private itemService: ItemService) {
  }

  ngOnInit(): void {
    this.itemService
    .getAllItems()
    .then(items => {
          this.allItems = items;
          this.afilteredItems = items
        }
    );

    this.itemService
    .getAllItemCategories()
    .then(itemCategories => this.itemCategories = this.itemCategories.concat(itemCategories));
  }

  onSelect(selectedCategory: any) {
    if (selectedCategory == "0") {
      this.afilteredItems = this.allItems;
    } else {
      this.afilteredItems = this.allItems.filter((item) => item.category == selectedCategory)
    }
  }
}
